@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>Mapas
           <a class="btn btn-success pull-right" href="{{ route('omaps.create') }}" style="background: #F3A699; border; border-color: #F3A699;"><i class="glyphicon glyphicon-plus"></i> Crear mapa</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($omaps->count())
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 5%">Id</th>
							<th style="width: 10%">Titulo</th>
                            <th style="width: 70%">Imágen</th>
                            <th style="width: 15%" class="text-right">Opciones</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($omaps as $omap)
                            <tr>
                                <td class="col-1" >{{$omap->id}}</td>
                                
                    <td class="col-3">{{$omap->title}}</td>
					<td class="col-5"><img class="img-responsive" src="/maps/{{$omap->image}}"></td>
                                <td class="col-3" class="text-right">
                                    <a class="btn btn-xs btn-primary" style="background: #F3A699; border; border-color: #F3A699;" href="{{ route('omaps.show', $omap->id) }}">Ver</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('omaps.edit', $omap->id) }}" style="background: #F3A699; border; border-color: #F3A699;">Editar</a>
                                    <form action="{{ route('omaps.destroy', $omap->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger" >Borrar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $omaps->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection