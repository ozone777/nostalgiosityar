@extends('layout')
@section('header')
<div class="page-header">
        <h1>Odestinations / Show #{{$odestination->id}}</h1>
        <form action="{{ route('odestinations.destroy', $odestination->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('odestinations.edit', $odestination->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="omap_id">OMAP_ID</label>
                     <p class="form-control-static">{{$odestination->omap_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="x">X</label>
                     <p class="form-control-static">{{$odestination->x}}</p>
                </div>
                    <div class="form-group">
                     <label for="y">Y</label>
                     <p class="form-control-static">{{$odestination->y}}</p>
                </div>
                    <div class="form-group">
                     <label for="z">Z</label>
                     <p class="form-control-static">{{$odestination->z}}</p>
                </div>
                    <div class="form-group">
                     <label for="title">TITLE</label>
                     <p class="form-control-static">{{$odestination->title}}</p>
                </div>
                    <div class="form-group">
                     <label for="url">URL</label>
                     <p class="form-control-static">{{$odestination->url}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('odestinations.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection