@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i>Editar / Destino #{{$odestination->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('odestinations.update', $odestination->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('omap_id')) has-error @endif">
		
                    <select name="omap_id" id="omap_id-field" class="form-control">
					@foreach($maps as $map)
		   			<option value="{{$map->id}}" @if($odestination->omap_id == $map->id) selected="selected" @endif>{{$map->title}}</option>
					@endforeach
					</select>
					   
                    </div>
                    <div class="form-group @if($errors->has('x')) has-error @endif">
                       <label for="x-field">X</label>
                    <input type="text" id="x-field" name="x" class="form-control" value="{{ is_null(old("x")) ? $odestination->x : old("x") }}"/>
                       @if($errors->has("x"))
                        <span class="help-block">{{ $errors->first("x") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('y')) has-error @endif">
                       <label for="y-field">Y</label>
                    <input type="text" id="y-field" name="y" class="form-control" value="{{ is_null(old("y")) ? $odestination->y : old("y") }}"/>
                       @if($errors->has("y"))
                        <span class="help-block">{{ $errors->first("y") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('z')) has-error @endif">
                       <label for="z-field">Z</label>
                    <input type="text" id="z-field" name="z" class="form-control" value="{{ is_null(old("z")) ? $odestination->z : old("z") }}"/>
                       @if($errors->has("z"))
                        <span class="help-block">{{ $errors->first("z") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ is_null(old("title")) ? $odestination->title : old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('url')) has-error @endif">
                       <label for="url-field">Url</label>
                    <textarea class="form-control" id="url-field" rows="3" name="url">{{ is_null(old("url")) ? $odestination->url : old("url") }}</textarea>
                       @if($errors->has("url"))
                        <span class="help-block">{{ $errors->first("url") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('odestinations.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
