@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>Destinos
            <a class="btn btn-success pull-right" href="{{ route('odestinations.create') }}" style="background: #F3A699; border; border-color: #F3A699;"><i class="glyphicon glyphicon-plus"></i> Crear destino</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
           
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Mapa</th>
                        <th>X</th>
                        <th>Y</th>
                        <th>Z</th>
                        <th>Titulo</th>
                        <th>URL</th>
                            <th class="text-right">Opciones</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($odestinations as $odestination)
                            <tr>
                                <td>{{$odestination->id}}</td>
                                <td>{{$odestination->map_title}}</td>
                    <td>{{$odestination->x}}</td>
                    <td>{{$odestination->y}}</td>
                    <td>{{$odestination->z}}</td>
                    <td>{{$odestination->title}}</td>
                    <td>{{$odestination->url}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('odestinations.show', $odestination->id) }}" style="background: #F3A699; border; border-color: #F3A699;"> Ver</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('odestinations.edit', $odestination->id) }}" style="background: #F3A699; border; border-color: #F3A699;">Editar</a>
                                    <form action="{{ route('odestinations.destroy', $odestination->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger">Borrar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
           

        </div>
    </div>

@endsection