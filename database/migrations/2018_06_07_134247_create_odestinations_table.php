<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOdestinationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('odestinations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('omap_id');
            $table->integer('x');
            $table->integer('y');
            $table->integer('z');
            $table->string('title');
            $table->text('url');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('odestinations');
	}

}
