<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::resource("odestinations","OdestinationController"); 
Route::resource("omaps","OmapController");
Route::auth();

Route::get('/get_map', 'BaseController@get_map');
Route::get('/get_destinations', 'BaseController@get_destinations');

Route::get('/', array('as' => 'odestinations.index', 'uses' => 'OdestinationController@index'));



