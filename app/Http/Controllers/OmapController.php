<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Omap;
use Illuminate\Http\Request;

class OmapController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
	public function index()
	{
		$omaps = Omap::orderBy('id', 'desc')->paginate(10);
		$tab = "maps";
		return view('omaps.index', compact('omaps','tab'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$tab = "maps";
		return view('omaps.create',compact('tab'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$omap = new Omap();
        $omap->title = $request->input("title");
		
		if ($request->file('filem')) {
		
			$file = $request->file('filem');
	        // SET UPLOAD PATH
	        $destinationPath = "maps";
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
			$fileName_solo = md5(uniqid(rand(), true));
	        $fileName = $fileName_solo . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$omap->image =  $fileName;
		}
		
		$omap->save();
		return redirect()->route('omaps.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$omap = Omap::findOrFail($id);
		$tab = "maps";
		return view('omaps.show', compact('omap','tab'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$omap = Omap::findOrFail($id);
		$tab = "maps";
		return view('omaps.edit', compact('omap','tab'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$omap = Omap::findOrFail($id);
        $omap->title = $request->input("title");
		
		if ($request->file('filem')) {
		
			$file = $request->file('filem');
	        // SET UPLOAD PATH
	        $destinationPath = "maps";
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
			$fileName_solo = md5(uniqid(rand(), true));
	        $fileName = $fileName_solo . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$omap->image =  $fileName;
		}

		$omap->save();

		return redirect()->route('omaps.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$omap = Omap::findOrFail($id);
		$omap->delete();

		return redirect()->route('omaps.index')->with('message', 'Item deleted successfully.');
	}

}
