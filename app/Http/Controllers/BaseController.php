<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Odestination;
use App\Omap;

class BaseController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function get_map()
    {
		$title = $_GET["map_title"];
		$map = Omap::where('title', '=' ,$title)->firstOrFail();
        return response()->json($map);
    }
	
    public function get_destinations()
    {
		$title = $_GET["map_title"];
		$map = Omap::where('title', '=' ,$title)->firstOrFail();
		$destinations = Odestination::where("omap_id", '=', $map->id)->get();
        return response()->json($destinations);
    }
}