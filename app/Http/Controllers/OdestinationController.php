<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Odestination;
use App\Omap;
use Illuminate\Http\Request;
use DB;

class OdestinationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
	
	public function index()
	{
		//$odestinations = Odestination::orderBy('id', 'desc')->paginate(10);
		$tab = "destinations";
		
		$odestinations = DB::select("SELECT d.*, m.title as map_title FROM odestinations d inner join omaps m on m.id = d.omap_id");
		
		return view('odestinations.index', compact('odestinations','tab'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$tab = "destinations";
		$maps = Omap::orderBy('id', 'desc')->get();
		return view('odestinations.create',compact('tab','maps'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$odestination = new Odestination();

		$odestination->omap_id = $request->input("omap_id");
        $odestination->x = $request->input("x");
        $odestination->y = $request->input("y");
        $odestination->z = $request->input("z");
        $odestination->title = $request->input("title");
        $odestination->url = $request->input("url");

		$odestination->save();

		return redirect()->route('odestinations.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$odestination = Odestination::findOrFail($id);
		$tab = "destinations";
		return view('odestinations.show', compact('odestination','tab'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$maps = Omap::orderBy('id', 'desc')->get();
		$odestination = Odestination::findOrFail($id);
		$tab = "destinations";
		return view('odestinations.edit', compact('odestination','tab','maps'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$odestination = Odestination::findOrFail($id);

		$odestination->omap_id = $request->input("omap_id");
        $odestination->x = $request->input("x");
        $odestination->y = $request->input("y");
        $odestination->z = $request->input("z");
        $odestination->title = $request->input("title");
        $odestination->url = $request->input("url");

		$odestination->save();

		return redirect()->route('odestinations.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$odestination = Odestination::findOrFail($id);
		$odestination->delete();

		return redirect()->route('odestinations.index')->with('message', 'Item deleted successfully.');
	}

}
